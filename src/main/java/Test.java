import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Test {

    public static void main(String[] args) throws IOException {
        Observable<Long> values = Observable.interval(1000, TimeUnit.MILLISECONDS);
        Disposable subscription = values.subscribe(
                v -> System.out.println("Received: " + v),
                e -> System.out.println("Error: " + e),
                () -> System.out.println("Completed")
        );
        System.in.read();
    }

}
