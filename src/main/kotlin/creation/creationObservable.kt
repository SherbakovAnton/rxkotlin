package creation

import io.reactivex.Observable
import java.util.concurrent.Callable
import java.util.concurrent.TimeUnit
import kotlin.concurrent.timer

val list = listOf(0,1,2,3,4,5)

fun main(args: Array<String>)  {

    createObservable().subscribe(::println)

    val deferObservable = deferTest()

    deferObservable.subscribe(::println)
    deferObservable.subscribe(::println)

    val justObservable = justTest()

    justObservable.subscribe(::println)
    justObservable.subscribe(::println)

    println("rangeTest")

    rangeTest().subscribe(::print)

    println()

//    println("interval")
//
//    intervalTest().subscribe(System.out::println)

    println("timer")
    timerTest().subscribe(::println)

    System.`in`.read()
}

//create
fun createObservable() : Observable<List<Int>>{
    return Observable.create<List<Int>> {

        it.onNext(list)
        it.onComplete()
    }
}

//defer
fun deferTest(): Observable<Long> {
    return Observable.defer ({ Observable.just(System.currentTimeMillis()) })
}

//just
fun justTest() : Observable<Long> {
    return Observable.just(System.currentTimeMillis())
}

//range
fun rangeTest() =
    Observable.range(10, 15)

//interval
fun intervalTest() = Observable.interval(100, TimeUnit.MILLISECONDS)

//timer

fun timerTest() = Observable.timer(1, TimeUnit.SECONDS)


